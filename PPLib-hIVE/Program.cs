﻿using System;
using System.Windows.Forms;
//
using System.Threading;

namespace PPLib_hIVE
{
    static class Program
    {

        private static Thread trFrmStartup;
        private static ThreadStart trsFrmStartup;

        private static void FrmStartup()
        {
            try
            {
                Application.Run(new FrmStartup());
            }
            catch { }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            trsFrmStartup = new ThreadStart(FrmStartup);
            trFrmStartup = new Thread(trsFrmStartup);
            trFrmStartup.Priority = ThreadPriority.Normal;
            //
            trFrmStartup.Start();
        }
    }
}
