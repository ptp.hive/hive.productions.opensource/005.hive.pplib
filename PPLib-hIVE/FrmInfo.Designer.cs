﻿namespace PPLib_hIVE
{
    partial class FrmInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInfo));
            this.tlpAboutUs = new System.Windows.Forms.TableLayoutPanel();
            this.tlpANP = new System.Windows.Forms.TableLayoutPanel();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblProgrammerName = new System.Windows.Forms.Label();
            this.linkLblEmail = new System.Windows.Forms.LinkLabel();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.tlpAboutUs.SuspendLayout();
            this.tlpANP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpAboutUs
            // 
            this.tlpAboutUs.ColumnCount = 1;
            this.tlpAboutUs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAboutUs.Controls.Add(this.tlpANP, 0, 2);
            this.tlpAboutUs.Controls.Add(this.picLogo, 0, 0);
            this.tlpAboutUs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAboutUs.Location = new System.Drawing.Point(0, 0);
            this.tlpAboutUs.Name = "tlpAboutUs";
            this.tlpAboutUs.Padding = new System.Windows.Forms.Padding(20);
            this.tlpAboutUs.RowCount = 3;
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tlpAboutUs.Size = new System.Drawing.Size(384, 461);
            this.tlpAboutUs.TabIndex = 3;
            // 
            // tlpANP
            // 
            this.tlpANP.ColumnCount = 1;
            this.tlpANP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpANP.Controls.Add(this.lblVersion, 0, 4);
            this.tlpANP.Controls.Add(this.lblProductName, 0, 0);
            this.tlpANP.Controls.Add(this.lblProgrammerName, 0, 1);
            this.tlpANP.Controls.Add(this.linkLblEmail, 0, 3);
            this.tlpANP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpANP.Font = new System.Drawing.Font("B Jadid", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tlpANP.Location = new System.Drawing.Point(23, 254);
            this.tlpANP.Name = "tlpANP";
            this.tlpANP.RowCount = 5;
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpANP.Size = new System.Drawing.Size(338, 184);
            this.tlpANP.TabIndex = 33;
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblVersion.Location = new System.Drawing.Point(3, 151);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(332, 25);
            this.lblVersion.TabIndex = 336;
            this.lblVersion.Text = "نسخه : ۱.۰۰";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProductName
            // 
            this.lblProductName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProductName.AutoSize = true;
            this.lblProductName.Font = new System.Drawing.Font("B Jadid", 12F, System.Drawing.FontStyle.Bold);
            this.lblProductName.ForeColor = System.Drawing.Color.Red;
            this.lblProductName.Location = new System.Drawing.Point(3, 9);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(332, 23);
            this.lblProductName.TabIndex = 312;
            this.lblProductName.Text = "کتابخانه کلمات عبور حمایت‌شده - کندو";
            this.lblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProgrammerName
            // 
            this.lblProgrammerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProgrammerName.AutoSize = true;
            this.lblProgrammerName.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblProgrammerName.ForeColor = System.Drawing.Color.Blue;
            this.lblProgrammerName.Location = new System.Drawing.Point(3, 49);
            this.lblProgrammerName.Name = "lblProgrammerName";
            this.lblProgrammerName.Size = new System.Drawing.Size(332, 25);
            this.lblProgrammerName.TabIndex = 313;
            this.lblProgrammerName.Text = "hIVE Productions - Mehdi MzITs";
            this.lblProgrammerName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // linkLblEmail
            // 
            this.linkLblEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLblEmail.AutoSize = true;
            this.linkLblEmail.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.linkLblEmail.LinkColor = System.Drawing.Color.DarkMagenta;
            this.linkLblEmail.Location = new System.Drawing.Point(3, 110);
            this.linkLblEmail.Name = "linkLblEmail";
            this.linkLblEmail.Size = new System.Drawing.Size(332, 25);
            this.linkLblEmail.TabIndex = 335;
            this.linkLblEmail.TabStop = true;
            this.linkLblEmail.Text = "hIVE7222@gmail.com";
            this.linkLblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picLogo
            // 
            this.picLogo.BackColor = System.Drawing.Color.Transparent;
            this.picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLogo.Image = global::PPLib_hIVE.Properties.Resources.hIVE_Company_Pro___Logo_T444x250___Main;
            this.picLogo.Location = new System.Drawing.Point(23, 23);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(338, 204);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 1;
            this.picLogo.TabStop = false;
            // 
            // FrmInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Cyan;
            this.ClientSize = new System.Drawing.Size(384, 461);
            this.Controls.Add(this.tlpAboutUs);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "درباره ما";
            this.Load += new System.EventHandler(this.FrmInfo_Load);
            this.tlpAboutUs.ResumeLayout(false);
            this.tlpANP.ResumeLayout(false);
            this.tlpANP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpAboutUs;
        private System.Windows.Forms.TableLayoutPanel tlpANP;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.Label lblProgrammerName;
        private System.Windows.Forms.LinkLabel linkLblEmail;
        private System.Windows.Forms.PictureBox picLogo;
    }
}