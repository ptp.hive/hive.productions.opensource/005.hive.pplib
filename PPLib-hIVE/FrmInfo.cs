﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PPLib_hIVE
{
    public partial class FrmInfo : Form
    {
        public FrmInfo()
        {
            InitializeComponent();
        }

        private void FrmInfo_Load(object sender, EventArgs e)
        {
            try
            {
                lblVersion.Text = "نسخه : " + Application.ProductVersion.Substring(0, 4);
            }
            catch { }
        }
    }
}
