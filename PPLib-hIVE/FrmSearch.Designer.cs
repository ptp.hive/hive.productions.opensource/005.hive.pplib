﻿namespace PPLib_hIVE
{
    partial class FrmSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearch));
            this.timerSearchPPLib = new System.Windows.Forms.Timer(this.components);
            this.bgwSearchPPLib = new System.ComponentModel.BackgroundWorker();
            this.toolStripStatus = new System.Windows.Forms.ToolStrip();
            this.ProgressBarStatusPersent = new System.Windows.Forms.ToolStripProgressBar();
            this.lblTimerStatusLasted = new System.Windows.Forms.ToolStripLabel();
            this.lblStatusPersentProcces = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.lblStatusPersentTrueProcces = new System.Windows.Forms.ToolStripLabel();
            this.tlpStartupForm = new System.Windows.Forms.TableLayoutPanel();
            this.tlpSearchLibPass = new System.Windows.Forms.TableLayoutPanel();
            this.btnSearchLibrary = new System.Windows.Forms.Button();
            this.cmbSrchSelectedProtectedAlgoritm = new System.Windows.Forms.ComboBox();
            this.tlpSrchNumericsCount = new System.Windows.Forms.TableLayoutPanel();
            this.numUDSrchNumericsCount = new System.Windows.Forms.NumericUpDown();
            this.lblSrchNumericsCount = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.tlpDetailPass = new System.Windows.Forms.TableLayoutPanel();
            this.txtPasswordHashed = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnPassHashedPaste = new System.Windows.Forms.Button();
            this.btnPassCopy = new System.Windows.Forms.Button();
            this.btnPassHashedDelete = new System.Windows.Forms.Button();
            this.toolStripStatus.SuspendLayout();
            this.tlpStartupForm.SuspendLayout();
            this.tlpSearchLibPass.SuspendLayout();
            this.tlpSrchNumericsCount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDSrchNumericsCount)).BeginInit();
            this.tlpDetailPass.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerSearchPPLib
            // 
            this.timerSearchPPLib.Interval = 1000;
            this.timerSearchPPLib.Tick += new System.EventHandler(this.timerSearchPPLib_Tick);
            // 
            // bgwSearchPPLib
            // 
            this.bgwSearchPPLib.WorkerReportsProgress = true;
            this.bgwSearchPPLib.WorkerSupportsCancellation = true;
            this.bgwSearchPPLib.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwSearchPPLib_DoWork);
            // 
            // toolStripStatus
            // 
            this.toolStripStatus.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.toolStripStatus, "toolStripStatus");
            this.toolStripStatus.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProgressBarStatusPersent,
            this.lblTimerStatusLasted,
            this.lblStatusPersentProcces,
            this.toolStripSeparator1,
            this.lblStatusPersentTrueProcces});
            this.toolStripStatus.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStripStatus.Name = "toolStripStatus";
            // 
            // ProgressBarStatusPersent
            // 
            this.ProgressBarStatusPersent.Name = "ProgressBarStatusPersent";
            resources.ApplyResources(this.ProgressBarStatusPersent, "ProgressBarStatusPersent");
            // 
            // lblTimerStatusLasted
            // 
            this.lblTimerStatusLasted.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblTimerStatusLasted.ForeColor = System.Drawing.Color.DarkBlue;
            this.lblTimerStatusLasted.Name = "lblTimerStatusLasted";
            resources.ApplyResources(this.lblTimerStatusLasted, "lblTimerStatusLasted");
            // 
            // lblStatusPersentProcces
            // 
            resources.ApplyResources(this.lblStatusPersentProcces, "lblStatusPersentProcces");
            this.lblStatusPersentProcces.ForeColor = System.Drawing.Color.Magenta;
            this.lblStatusPersentProcces.Name = "lblStatusPersentProcces";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // lblStatusPersentTrueProcces
            // 
            resources.ApplyResources(this.lblStatusPersentTrueProcces, "lblStatusPersentTrueProcces");
            this.lblStatusPersentTrueProcces.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblStatusPersentTrueProcces.Name = "lblStatusPersentTrueProcces";
            // 
            // tlpStartupForm
            // 
            resources.ApplyResources(this.tlpStartupForm, "tlpStartupForm");
            this.tlpStartupForm.Controls.Add(this.tlpSearchLibPass, 2, 1);
            this.tlpStartupForm.Controls.Add(this.btnClose, 0, 0);
            this.tlpStartupForm.Controls.Add(this.tlpDetailPass, 1, 1);
            this.tlpStartupForm.Name = "tlpStartupForm";
            // 
            // tlpSearchLibPass
            // 
            resources.ApplyResources(this.tlpSearchLibPass, "tlpSearchLibPass");
            this.tlpSearchLibPass.Controls.Add(this.btnSearchLibrary, 1, 1);
            this.tlpSearchLibPass.Controls.Add(this.cmbSrchSelectedProtectedAlgoritm, 1, 3);
            this.tlpSearchLibPass.Controls.Add(this.tlpSrchNumericsCount, 1, 2);
            this.tlpSearchLibPass.Name = "tlpSearchLibPass";
            // 
            // btnSearchLibrary
            // 
            resources.ApplyResources(this.btnSearchLibrary, "btnSearchLibrary");
            this.btnSearchLibrary.BackColor = System.Drawing.Color.Magenta;
            this.btnSearchLibrary.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearchLibrary.ForeColor = System.Drawing.Color.Navy;
            this.btnSearchLibrary.Name = "btnSearchLibrary";
            this.btnSearchLibrary.UseVisualStyleBackColor = false;
            this.btnSearchLibrary.Click += new System.EventHandler(this.btnSearchLibrary_Click);
            // 
            // cmbSrchSelectedProtectedAlgoritm
            // 
            resources.ApplyResources(this.cmbSrchSelectedProtectedAlgoritm, "cmbSrchSelectedProtectedAlgoritm");
            this.cmbSrchSelectedProtectedAlgoritm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSrchSelectedProtectedAlgoritm.FormattingEnabled = true;
            this.cmbSrchSelectedProtectedAlgoritm.Items.AddRange(new object[] {
            resources.GetString("cmbSrchSelectedProtectedAlgoritm.Items"),
            resources.GetString("cmbSrchSelectedProtectedAlgoritm.Items1"),
            resources.GetString("cmbSrchSelectedProtectedAlgoritm.Items2")});
            this.cmbSrchSelectedProtectedAlgoritm.Name = "cmbSrchSelectedProtectedAlgoritm";
            // 
            // tlpSrchNumericsCount
            // 
            resources.ApplyResources(this.tlpSrchNumericsCount, "tlpSrchNumericsCount");
            this.tlpSrchNumericsCount.Controls.Add(this.numUDSrchNumericsCount, 1, 0);
            this.tlpSrchNumericsCount.Controls.Add(this.lblSrchNumericsCount, 0, 0);
            this.tlpSrchNumericsCount.Name = "tlpSrchNumericsCount";
            // 
            // numUDSrchNumericsCount
            // 
            resources.ApplyResources(this.numUDSrchNumericsCount, "numUDSrchNumericsCount");
            this.numUDSrchNumericsCount.ForeColor = System.Drawing.Color.MidnightBlue;
            this.numUDSrchNumericsCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUDSrchNumericsCount.Name = "numUDSrchNumericsCount";
            this.numUDSrchNumericsCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblSrchNumericsCount
            // 
            resources.ApplyResources(this.lblSrchNumericsCount, "lblSrchNumericsCount");
            this.lblSrchNumericsCount.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblSrchNumericsCount.Name = "lblSrchNumericsCount";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.BackColor = System.Drawing.Color.Red;
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tlpDetailPass
            // 
            resources.ApplyResources(this.tlpDetailPass, "tlpDetailPass");
            this.tlpDetailPass.Controls.Add(this.txtPasswordHashed, 1, 1);
            this.tlpDetailPass.Controls.Add(this.txtPassword, 1, 3);
            this.tlpDetailPass.Controls.Add(this.btnPassHashedPaste, 0, 1);
            this.tlpDetailPass.Controls.Add(this.btnPassCopy, 0, 3);
            this.tlpDetailPass.Controls.Add(this.btnPassHashedDelete, 2, 1);
            this.tlpDetailPass.Name = "tlpDetailPass";
            // 
            // txtPasswordHashed
            // 
            resources.ApplyResources(this.txtPasswordHashed, "txtPasswordHashed");
            this.txtPasswordHashed.BackColor = System.Drawing.Color.Gold;
            this.txtPasswordHashed.ForeColor = System.Drawing.Color.Maroon;
            this.txtPasswordHashed.Name = "txtPasswordHashed";
            // 
            // txtPassword
            // 
            resources.ApplyResources(this.txtPassword, "txtPassword");
            this.txtPassword.BackColor = System.Drawing.Color.Azure;
            this.txtPassword.ForeColor = System.Drawing.Color.Blue;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.ReadOnly = true;
            // 
            // btnPassHashedPaste
            // 
            resources.ApplyResources(this.btnPassHashedPaste, "btnPassHashedPaste");
            this.btnPassHashedPaste.BackColor = System.Drawing.Color.Yellow;
            this.btnPassHashedPaste.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPassHashedPaste.ForeColor = System.Drawing.Color.Red;
            this.btnPassHashedPaste.Name = "btnPassHashedPaste";
            this.btnPassHashedPaste.UseVisualStyleBackColor = false;
            this.btnPassHashedPaste.Click += new System.EventHandler(this.btnPassHashedPaste_Click);
            // 
            // btnPassCopy
            // 
            resources.ApplyResources(this.btnPassCopy, "btnPassCopy");
            this.btnPassCopy.BackColor = System.Drawing.Color.LimeGreen;
            this.btnPassCopy.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPassCopy.ForeColor = System.Drawing.Color.Red;
            this.btnPassCopy.Name = "btnPassCopy";
            this.btnPassCopy.UseVisualStyleBackColor = false;
            this.btnPassCopy.Click += new System.EventHandler(this.btnPassCopy_Click);
            // 
            // btnPassHashedDelete
            // 
            resources.ApplyResources(this.btnPassHashedDelete, "btnPassHashedDelete");
            this.btnPassHashedDelete.BackColor = System.Drawing.Color.Yellow;
            this.btnPassHashedDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPassHashedDelete.ForeColor = System.Drawing.Color.Red;
            this.btnPassHashedDelete.Name = "btnPassHashedDelete";
            this.btnPassHashedDelete.UseVisualStyleBackColor = false;
            this.btnPassHashedDelete.Click += new System.EventHandler(this.btnPassHashedDelete_Click);
            // 
            // FrmSearch
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ControlBox = false;
            this.Controls.Add(this.tlpStartupForm);
            this.Controls.Add(this.toolStripStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSearch";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SearchForm_FormClosing);
            this.Load += new System.EventHandler(this.SearchForm_Load);
            this.Click += new System.EventHandler(this.SearchForm_Load);
            this.toolStripStatus.ResumeLayout(false);
            this.toolStripStatus.PerformLayout();
            this.tlpStartupForm.ResumeLayout(false);
            this.tlpStartupForm.PerformLayout();
            this.tlpSearchLibPass.ResumeLayout(false);
            this.tlpSrchNumericsCount.ResumeLayout(false);
            this.tlpSrchNumericsCount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDSrchNumericsCount)).EndInit();
            this.tlpDetailPass.ResumeLayout(false);
            this.tlpDetailPass.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timerSearchPPLib;
        private System.ComponentModel.BackgroundWorker bgwSearchPPLib;
        private System.Windows.Forms.ToolStrip toolStripStatus;
        private System.Windows.Forms.ToolStripProgressBar ProgressBarStatusPersent;
        private System.Windows.Forms.ToolStripLabel lblTimerStatusLasted;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TableLayoutPanel tlpStartupForm;
        private System.Windows.Forms.TableLayoutPanel tlpSearchLibPass;
        private System.Windows.Forms.Button btnSearchLibrary;
        private System.Windows.Forms.ComboBox cmbSrchSelectedProtectedAlgoritm;
        private System.Windows.Forms.TableLayoutPanel tlpSrchNumericsCount;
        private System.Windows.Forms.NumericUpDown numUDSrchNumericsCount;
        private System.Windows.Forms.Label lblSrchNumericsCount;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TableLayoutPanel tlpDetailPass;
        private System.Windows.Forms.TextBox txtPasswordHashed;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnPassHashedPaste;
        private System.Windows.Forms.Button btnPassCopy;
        private System.Windows.Forms.Button btnPassHashedDelete;
        private System.Windows.Forms.ToolStripLabel lblStatusPersentProcces;
        private System.Windows.Forms.ToolStripLabel lblStatusPersentTrueProcces;
    }
}