﻿namespace PPLib_hIVE
{
    partial class FrmErrors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpFormErrors = new System.Windows.Forms.TableLayoutPanel();
            this.rtbCEM_MesErrorsForm_NotFriendly = new System.Windows.Forms.RichTextBox();
            this.btnErrorsSave = new System.Windows.Forms.Button();
            this.rtbCEM_MesErrorsForm_Friendly = new System.Windows.Forms.RichTextBox();
            this.tlpFormErrors.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpFormErrors
            // 
            this.tlpFormErrors.ColumnCount = 3;
            this.tlpFormErrors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpFormErrors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tlpFormErrors.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tlpFormErrors.Controls.Add(this.rtbCEM_MesErrorsForm_NotFriendly, 2, 1);
            this.tlpFormErrors.Controls.Add(this.btnErrorsSave, 0, 0);
            this.tlpFormErrors.Controls.Add(this.rtbCEM_MesErrorsForm_Friendly, 1, 1);
            this.tlpFormErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpFormErrors.Location = new System.Drawing.Point(0, 0);
            this.tlpFormErrors.Name = "tlpFormErrors";
            this.tlpFormErrors.Padding = new System.Windows.Forms.Padding(35, 5, 5, 25);
            this.tlpFormErrors.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tlpFormErrors.RowCount = 2;
            this.tlpFormErrors.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tlpFormErrors.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88F));
            this.tlpFormErrors.Size = new System.Drawing.Size(760, 437);
            this.tlpFormErrors.TabIndex = 58;
            // 
            // rtbCEM_MesErrorsForm_NotFriendly
            // 
            this.rtbCEM_MesErrorsForm_NotFriendly.BackColor = System.Drawing.SystemColors.HighlightText;
            this.rtbCEM_MesErrorsForm_NotFriendly.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbCEM_MesErrorsForm_NotFriendly.Location = new System.Drawing.Point(51, 69);
            this.rtbCEM_MesErrorsForm_NotFriendly.Margin = new System.Windows.Forms.Padding(8, 16, 16, 16);
            this.rtbCEM_MesErrorsForm_NotFriendly.Name = "rtbCEM_MesErrorsForm_NotFriendly";
            this.rtbCEM_MesErrorsForm_NotFriendly.ReadOnly = true;
            this.rtbCEM_MesErrorsForm_NotFriendly.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtbCEM_MesErrorsForm_NotFriendly.Size = new System.Drawing.Size(300, 327);
            this.rtbCEM_MesErrorsForm_NotFriendly.TabIndex = 4;
            this.rtbCEM_MesErrorsForm_NotFriendly.Text = "";
            // 
            // btnErrorsSave
            // 
            this.btnErrorsSave.AutoSize = true;
            this.btnErrorsSave.BackColor = System.Drawing.Color.SeaShell;
            this.btnErrorsSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnErrorsSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnErrorsSave.Font = new System.Drawing.Font("B Jadid", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnErrorsSave.ForeColor = System.Drawing.Color.Red;
            this.btnErrorsSave.Location = new System.Drawing.Point(693, 10);
            this.btnErrorsSave.Margin = new System.Windows.Forms.Padding(5, 5, 10, 10);
            this.btnErrorsSave.Name = "btnErrorsSave";
            this.btnErrorsSave.Size = new System.Drawing.Size(57, 33);
            this.btnErrorsSave.TabIndex = 2;
            this.btnErrorsSave.Text = "ذخیره";
            this.btnErrorsSave.UseVisualStyleBackColor = false;
            this.btnErrorsSave.Click += new System.EventHandler(this.btnErrorsSave_Click);
            // 
            // rtbCEM_MesErrorsForm_Friendly
            // 
            this.rtbCEM_MesErrorsForm_Friendly.BackColor = System.Drawing.SystemColors.HighlightText;
            this.rtbCEM_MesErrorsForm_Friendly.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbCEM_MesErrorsForm_Friendly.Location = new System.Drawing.Point(367, 69);
            this.rtbCEM_MesErrorsForm_Friendly.Margin = new System.Windows.Forms.Padding(16, 16, 8, 16);
            this.rtbCEM_MesErrorsForm_Friendly.Name = "rtbCEM_MesErrorsForm_Friendly";
            this.rtbCEM_MesErrorsForm_Friendly.ReadOnly = true;
            this.rtbCEM_MesErrorsForm_Friendly.Size = new System.Drawing.Size(300, 327);
            this.rtbCEM_MesErrorsForm_Friendly.TabIndex = 3;
            this.rtbCEM_MesErrorsForm_Friendly.Text = "";
            // 
            // FrmErrors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(760, 437);
            this.Controls.Add(this.tlpFormErrors);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FrmErrors";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Errors Forms";
            this.Load += new System.EventHandler(this.FrmErrors_Load);
            this.tlpFormErrors.ResumeLayout(false);
            this.tlpFormErrors.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpFormErrors;
        private System.Windows.Forms.Button btnErrorsSave;
        private System.Windows.Forms.RichTextBox rtbCEM_MesErrorsForm_Friendly;
        private System.Windows.Forms.RichTextBox rtbCEM_MesErrorsForm_NotFriendly;
    }
}