﻿namespace PPLib_hIVE
{
    partial class FrmStartup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStartup));
            this.toolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnExit = new System.Windows.Forms.ToolStripButton();
            this.btnMinimized = new System.Windows.Forms.ToolStripButton();
            this.btnInfo = new System.Windows.Forms.ToolStripButton();
            this.toolStripLogo = new System.Windows.Forms.ToolStrip();
            this.lblProductLogo = new System.Windows.Forms.ToolStripLabel();
            this.lblMainProductName = new System.Windows.Forms.ToolStripLabel();
            this.timerCreatePPLib = new System.Windows.Forms.Timer(this.components);
            this.tlpStartupForm = new System.Windows.Forms.TableLayoutPanel();
            this.picBoxLogo = new System.Windows.Forms.PictureBox();
            this.tlpCreateLibPass = new System.Windows.Forms.TableLayoutPanel();
            this.btnCreatePPLib = new System.Windows.Forms.Button();
            this.btnSearchLibrary = new System.Windows.Forms.Button();
            this.tlpNumericsCount = new System.Windows.Forms.TableLayoutPanel();
            this.numUDNumericsCount = new System.Windows.Forms.NumericUpDown();
            this.lblNumericsCount = new System.Windows.Forms.Label();
            this.cmbSelectedProtectedAlgoritm = new System.Windows.Forms.ComboBox();
            this.ProgressBarStatusPersent = new System.Windows.Forms.ToolStripProgressBar();
            this.lblTimerStatusLasted = new System.Windows.Forms.ToolStripLabel();
            this.toolStripStatus = new System.Windows.Forms.ToolStrip();
            this.lblStatusPersentProcces = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.lblStatusPersentTrueProcces = new System.Windows.Forms.ToolStripLabel();
            this.bgwCreatePPLib = new System.ComponentModel.BackgroundWorker();
            this.toolStripMenu.SuspendLayout();
            this.toolStripLogo.SuspendLayout();
            this.tlpStartupForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogo)).BeginInit();
            this.tlpCreateLibPass.SuspendLayout();
            this.tlpNumericsCount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDNumericsCount)).BeginInit();
            this.toolStripStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMenu
            // 
            this.toolStripMenu.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnExit,
            this.btnMinimized,
            this.btnInfo});
            resources.ApplyResources(this.toolStripMenu, "toolStripMenu");
            this.toolStripMenu.Name = "toolStripMenu";
            // 
            // btnExit
            // 
            this.btnExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnExit.Image = global::PPLib_hIVE.Properties.Resources.btnExit;
            resources.ApplyResources(this.btnExit, "btnExit");
            this.btnExit.Name = "btnExit";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnMinimized
            // 
            this.btnMinimized.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMinimized.Image = global::PPLib_hIVE.Properties.Resources.btnMinimized;
            resources.ApplyResources(this.btnMinimized, "btnMinimized");
            this.btnMinimized.Name = "btnMinimized";
            this.btnMinimized.Click += new System.EventHandler(this.btnMinimized_Click);
            // 
            // btnInfo
            // 
            this.btnInfo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnInfo.Image = global::PPLib_hIVE.Properties.Resources.btnInfo;
            resources.ApplyResources(this.btnInfo, "btnInfo");
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // toolStripLogo
            // 
            this.toolStripLogo.BackColor = System.Drawing.Color.Transparent;
            this.toolStripLogo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripLogo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblProductLogo,
            this.lblMainProductName});
            resources.ApplyResources(this.toolStripLogo, "toolStripLogo");
            this.toolStripLogo.Name = "toolStripLogo";
            this.toolStripLogo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseDown);
            this.toolStripLogo.MouseHover += new System.EventHandler(this.toolStripLogo_MouseHover);
            this.toolStripLogo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseMove);
            // 
            // lblProductLogo
            // 
            resources.ApplyResources(this.lblProductLogo, "lblProductLogo");
            this.lblProductLogo.BackgroundImage = global::PPLib_hIVE.Properties.Resources.lblLogo;
            this.lblProductLogo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lblProductLogo.ForeColor = System.Drawing.Color.Red;
            this.lblProductLogo.Name = "lblProductLogo";
            this.lblProductLogo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseDown);
            this.lblProductLogo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseMove);
            // 
            // lblMainProductName
            // 
            resources.ApplyResources(this.lblMainProductName, "lblMainProductName");
            this.lblMainProductName.ForeColor = System.Drawing.Color.Red;
            this.lblMainProductName.Name = "lblMainProductName";
            this.lblMainProductName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseDown);
            this.lblMainProductName.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseMove);
            // 
            // timerCreatePPLib
            // 
            this.timerCreatePPLib.Interval = 1000;
            this.timerCreatePPLib.Tick += new System.EventHandler(this.timerCreatePPLib_Tick);
            // 
            // tlpStartupForm
            // 
            resources.ApplyResources(this.tlpStartupForm, "tlpStartupForm");
            this.tlpStartupForm.Controls.Add(this.picBoxLogo, 0, 0);
            this.tlpStartupForm.Controls.Add(this.tlpCreateLibPass, 2, 0);
            this.tlpStartupForm.Name = "tlpStartupForm";
            // 
            // picBoxLogo
            // 
            resources.ApplyResources(this.picBoxLogo, "picBoxLogo");
            this.picBoxLogo.Image = global::PPLib_hIVE.Properties.Resources.hIVE_Company_Pro___Logo_T444x250___Main;
            this.picBoxLogo.Name = "picBoxLogo";
            this.picBoxLogo.TabStop = false;
            // 
            // tlpCreateLibPass
            // 
            resources.ApplyResources(this.tlpCreateLibPass, "tlpCreateLibPass");
            this.tlpCreateLibPass.Controls.Add(this.btnCreatePPLib, 1, 2);
            this.tlpCreateLibPass.Controls.Add(this.btnSearchLibrary, 1, 4);
            this.tlpCreateLibPass.Controls.Add(this.tlpNumericsCount, 1, 0);
            this.tlpCreateLibPass.Controls.Add(this.cmbSelectedProtectedAlgoritm, 1, 1);
            this.tlpCreateLibPass.Name = "tlpCreateLibPass";
            // 
            // btnCreatePPLib
            // 
            this.btnCreatePPLib.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnCreatePPLib.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnCreatePPLib, "btnCreatePPLib");
            this.btnCreatePPLib.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnCreatePPLib.Name = "btnCreatePPLib";
            this.btnCreatePPLib.UseVisualStyleBackColor = false;
            this.btnCreatePPLib.Click += new System.EventHandler(this.btnCreatePPLib_Click);
            // 
            // btnSearchLibrary
            // 
            this.btnSearchLibrary.BackColor = System.Drawing.Color.Magenta;
            this.btnSearchLibrary.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.btnSearchLibrary, "btnSearchLibrary");
            this.btnSearchLibrary.ForeColor = System.Drawing.Color.Navy;
            this.btnSearchLibrary.Name = "btnSearchLibrary";
            this.btnSearchLibrary.UseVisualStyleBackColor = false;
            this.btnSearchLibrary.Click += new System.EventHandler(this.btnSearchLibrary_Click);
            // 
            // tlpNumericsCount
            // 
            resources.ApplyResources(this.tlpNumericsCount, "tlpNumericsCount");
            this.tlpNumericsCount.Controls.Add(this.numUDNumericsCount, 1, 0);
            this.tlpNumericsCount.Controls.Add(this.lblNumericsCount, 0, 0);
            this.tlpNumericsCount.Name = "tlpNumericsCount";
            // 
            // numUDNumericsCount
            // 
            resources.ApplyResources(this.numUDNumericsCount, "numUDNumericsCount");
            this.numUDNumericsCount.ForeColor = System.Drawing.Color.MidnightBlue;
            this.numUDNumericsCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUDNumericsCount.Name = "numUDNumericsCount";
            this.numUDNumericsCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblNumericsCount
            // 
            resources.ApplyResources(this.lblNumericsCount, "lblNumericsCount");
            this.lblNumericsCount.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblNumericsCount.Name = "lblNumericsCount";
            // 
            // cmbSelectedProtectedAlgoritm
            // 
            resources.ApplyResources(this.cmbSelectedProtectedAlgoritm, "cmbSelectedProtectedAlgoritm");
            this.cmbSelectedProtectedAlgoritm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSelectedProtectedAlgoritm.FormattingEnabled = true;
            this.cmbSelectedProtectedAlgoritm.Items.AddRange(new object[] {
            resources.GetString("cmbSelectedProtectedAlgoritm.Items"),
            resources.GetString("cmbSelectedProtectedAlgoritm.Items1"),
            resources.GetString("cmbSelectedProtectedAlgoritm.Items2")});
            this.cmbSelectedProtectedAlgoritm.Name = "cmbSelectedProtectedAlgoritm";
            // 
            // ProgressBarStatusPersent
            // 
            this.ProgressBarStatusPersent.Name = "ProgressBarStatusPersent";
            resources.ApplyResources(this.ProgressBarStatusPersent, "ProgressBarStatusPersent");
            // 
            // lblTimerStatusLasted
            // 
            this.lblTimerStatusLasted.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblTimerStatusLasted.ForeColor = System.Drawing.Color.DarkBlue;
            this.lblTimerStatusLasted.Name = "lblTimerStatusLasted";
            resources.ApplyResources(this.lblTimerStatusLasted, "lblTimerStatusLasted");
            // 
            // toolStripStatus
            // 
            this.toolStripStatus.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.toolStripStatus, "toolStripStatus");
            this.toolStripStatus.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProgressBarStatusPersent,
            this.lblTimerStatusLasted,
            this.lblStatusPersentProcces,
            this.toolStripSeparator1,
            this.lblStatusPersentTrueProcces});
            this.toolStripStatus.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStripStatus.Name = "toolStripStatus";
            // 
            // lblStatusPersentProcces
            // 
            resources.ApplyResources(this.lblStatusPersentProcces, "lblStatusPersentProcces");
            this.lblStatusPersentProcces.ForeColor = System.Drawing.Color.Magenta;
            this.lblStatusPersentProcces.Name = "lblStatusPersentProcces";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // lblStatusPersentTrueProcces
            // 
            resources.ApplyResources(this.lblStatusPersentTrueProcces, "lblStatusPersentTrueProcces");
            this.lblStatusPersentTrueProcces.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblStatusPersentTrueProcces.Name = "lblStatusPersentTrueProcces";
            // 
            // bgwCreatePPLib
            // 
            this.bgwCreatePPLib.WorkerReportsProgress = true;
            this.bgwCreatePPLib.WorkerSupportsCancellation = true;
            this.bgwCreatePPLib.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwCreatePPLib_DoWork);
            // 
            // FrmStartup
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkTurquoise;
            this.ControlBox = false;
            this.Controls.Add(this.tlpStartupForm);
            this.Controls.Add(this.toolStripStatus);
            this.Controls.Add(this.toolStripLogo);
            this.Controls.Add(this.toolStripMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FrmStartup";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StartupForm_FormClosing);
            this.Load += new System.EventHandler(this.StartupForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmDragForm_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragForm_MouseMove);
            this.toolStripMenu.ResumeLayout(false);
            this.toolStripMenu.PerformLayout();
            this.toolStripLogo.ResumeLayout(false);
            this.toolStripLogo.PerformLayout();
            this.tlpStartupForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogo)).EndInit();
            this.tlpCreateLibPass.ResumeLayout(false);
            this.tlpNumericsCount.ResumeLayout(false);
            this.tlpNumericsCount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDNumericsCount)).EndInit();
            this.toolStripStatus.ResumeLayout(false);
            this.toolStripStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripMenu;
        private System.Windows.Forms.ToolStripButton btnExit;
        private System.Windows.Forms.ToolStripButton btnMinimized;
        private System.Windows.Forms.ToolStripButton btnInfo;
        private System.Windows.Forms.ToolStrip toolStripLogo;
        private System.Windows.Forms.ToolStripLabel lblProductLogo;
        private System.Windows.Forms.ToolStripLabel lblMainProductName;
        private System.Windows.Forms.Timer timerCreatePPLib;
        private System.Windows.Forms.TableLayoutPanel tlpStartupForm;
        private System.Windows.Forms.PictureBox picBoxLogo;
        private System.Windows.Forms.TableLayoutPanel tlpCreateLibPass;
        private System.Windows.Forms.Button btnCreatePPLib;
        private System.Windows.Forms.Button btnSearchLibrary;
        private System.Windows.Forms.ComboBox cmbSelectedProtectedAlgoritm;
        private System.Windows.Forms.TableLayoutPanel tlpNumericsCount;
        private System.Windows.Forms.NumericUpDown numUDNumericsCount;
        private System.Windows.Forms.Label lblNumericsCount;
        private System.Windows.Forms.ToolStripProgressBar ProgressBarStatusPersent;
        private System.Windows.Forms.ToolStripLabel lblTimerStatusLasted;
        private System.Windows.Forms.ToolStrip toolStripStatus;
        private System.ComponentModel.BackgroundWorker bgwCreatePPLib;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel lblStatusPersentProcces;
        private System.Windows.Forms.ToolStripLabel lblStatusPersentTrueProcces;
    }
}

