﻿using System;
using System.Text;
//
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace PPLib_hIVE.Models_CS
{
    class ProtectedPassLibrary : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region ConnectionModels
        //----------

        private ConnectionDBMain _ConDBMain = new ConnectionDBMain();

        private TryCatch _TryCatch = new TryCatch();
        private string MesExpErrorForms = string.Empty;
        private string MesExpErrorForms_NotFriendly = string.Empty;

        //----------
        #endregion ConnectionModels

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        #region Create Default Value
        //----------

        private int NumericsCount = -1;
        private int BeforeNumericsCount = -1;
        //
        private int Chf_intStart = -1;
        private int Chf_intEnd = -1;
        //
        private string tblNameLibraryPass = "LibraryPass_";
        private string NewCharValue = string.Empty;
        private string NewCharPass = string.Empty;
        private string NewCharPassHash = string.Empty;
        private string NewLibp_ID = string.Empty;
        //
        private static string Q_ChfLoad = "SELECT * FROM CharFilterPass" +
                                          " WHERE Chf_Visible=1" +
                                          " ORDER by Chf_ID ASC;";

        //----------
        #endregion Create Default Value

        #region Create Protected Algoritms
        //----------

        public string GetCreateSumChf_int(int tblLibPassCount, int NCount)
        {
            string MesExpErrorForms = string.Empty;

            SettingsMain.Default.SumChf_int = 0;

            try
            {
                _ConDBMain.GetConMain.Open();

                SqlCommand Chfcom = new SqlCommand(Q_ChfLoad, _ConDBMain.GetConMain);
                try
                {
                    SqlDataReader Chfdr = Chfcom.ExecuteReader();

                    while (Chfdr.Read())
                    {
                        Chf_intStart = Convert.ToInt32(Chfdr["Chf_intStart"].ToString());
                        Chf_intEnd = Convert.ToInt32(Chfdr["Chf_intEnd"].ToString());

                        SettingsMain.Default.SumChf_int += (Chf_intEnd - Chf_intStart + 1);
                    }
                    Chfdr.Close();

                    int FactSumChf_int = 0;
                    for (int i = 1; i <= NCount; i++)
                    {
                        FactSumChf_int += (int)Math.Pow(SettingsMain.Default.SumChf_int, i);
                    }

                    SettingsMain.Default.SumChf_int = FactSumChf_int * tblLibPassCount;
                }
                catch (Exception exp)
                {
                    MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۱۰۱۰: " + exp.Message;

                    if (!_TryCatch.GetShowFriendlyMessage)
                    {
                        MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۱۰۱۰: " + exp.Message;
                    }
                    else
                    {
                        MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۱۰۱۰: " + "خطا در بارگیری اطلاعات کلمات عبور.   ";
                    }
                }

                _ConDBMain.GetConMain.Close();
            }
            catch (Exception exp)
            {
                MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۱۰۰۰: " + exp.Message;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۱۰۰۰: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۱۰۰۰: " + _TryCatch.GetMEC_ConDB;
                }

                try
                {
                    _ConDBMain.GetConMain.Close();
                }
                catch { }
            }

            return MesExpErrorForms;
        }

        private string GetStrToMD5(string StringValue)
        {
            try
            {
                MD5 MD5Str = new MD5CryptoServiceProvider();
                MD5Str.ComputeHash(ASCIIEncoding.Unicode.GetBytes(StringValue));
                byte[] hashDataStr = MD5Str.Hash;

                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < hashDataStr.Length; i++)
                {
                    strBuilder.Append(hashDataStr[i].ToString("x2"));
                }
                StringValue = strBuilder.ToString();
            }
            catch (Exception exp)
            {
                MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۰۰۱: " + exp.Message;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۰۰۱: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۰۰۱: " + "خطا در ایجاد هش کلمه عبور '" + StringValue + "' .   ";
                }

                return null;
            }

            return StringValue;
        }

        private string GetStrToSHA1(string StringValue)
        {
            try
            {
                SHA1 SHA1Str = new SHA1CryptoServiceProvider();
                SHA1Str.ComputeHash(ASCIIEncoding.Unicode.GetBytes(StringValue));
                byte[] hashDataStr = SHA1Str.Hash;

                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < hashDataStr.Length; i++)
                {
                    strBuilder.Append(hashDataStr[i].ToString("x2"));
                }
                StringValue = strBuilder.ToString();
            }
            catch (Exception exp)
            {
                MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۰۰۲: " + exp.Message;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۰۰۲: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۰۰۲: " + "خطا در ایجاد هش کلمه عبور '" + StringValue + "' .   ";
                }

                return null;
            }

            return StringValue;
        }

        private string CNewTableLibPass_NCount(string tblNameLibPass, int NCount)
        {
            try
            {
                string Q_tblLibPassCreate = "CREATE TABLE " + tblNameLibPass + NCount +
                                           " (Libp_ID NVARCHAR(512) NOT NULL PRIMARY KEY," +
                                            " Libp_CharPassword NVARCHAR(MAX) NOT NULL," +
                                            " Libp_CharPasswordHashed NVARCHAR(255) NOT NULL )";

                return Q_tblLibPassCreate;
            }
            catch (Exception exp)
            {
                MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۰۵۳: " + exp.Message;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۰۵۳: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۰۵۳: " + "خطا در ایجاد جدول کلمات عبور " + NCount + " رقمی.   ";
                }

                return null;
            }
        }

        public bool CreateMD5Library(int GetNumericsCount)
        {
            bool noError = true;
            tblNameLibraryPass = "MD5LibraryPass_";

            MesExpErrorForms = "";

            try
            {
                _ConDBMain.GetConMain.Open();

                #region Load 'New Numerics Count'
                //----------

                for (NumericsCount = 1; NumericsCount <= GetNumericsCount; NumericsCount++)
                {
                    BeforeNumericsCount = NumericsCount - 1;
                    string Q_LibpLoad = "SELECT * FROM " + tblNameLibraryPass + BeforeNumericsCount +
                                       " ORDER by Libp_ID ASC;";

                    try
                    {
                        #region Create 'New tblLibraryPass_NumericsCount'
                        //----------

                        string Q_tblLibPassCreate = CNewTableLibPass_NCount(tblNameLibraryPass, NumericsCount);

                        SqlCommand tblLibPCcom = new SqlCommand(Q_tblLibPassCreate, _ConDBMain.GetConMain);

                        try
                        {
                            tblLibPCcom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            noError = false;

                            MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۱۵۱: " + exp.Message;

                            if (!_TryCatch.GetShowFriendlyMessage)
                            {
                                MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۱۵۱: " + exp.Message;
                            }
                            else
                            {
                                MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۱۵۱: " + "خطا در ایجاد جدول کلمات عبور " + NumericsCount + " رقمی.   ";
                            }
                        }

                        //----------
                        #endregion Create 'New tblLibraryPass_NumericsCount'

                        SqlCommand Chfcom = new SqlCommand(Q_ChfLoad, _ConDBMain.GetConMain);
                        try
                        {
                            SqlDataReader Chfdr = Chfcom.ExecuteReader();

                            while (Chfdr.Read())
                            {
                                Chf_intStart = Convert.ToInt32(Chfdr["Chf_intStart"].ToString());
                                Chf_intEnd = Convert.ToInt32(Chfdr["Chf_intEnd"].ToString());

                                NewCharValue = string.Empty;

                                for (int NewChp = Chf_intStart; NewChp <= Chf_intEnd; NewChp++)
                                {
                                    // ----- Create 'New Numeric Count'
                                    NewCharValue = Convert.ToChar(NewChp).ToString();

                                    #region Load 'Before Numerics Count'
                                    //----------

                                    SqlCommand Libpcom = new SqlCommand(Q_LibpLoad, _ConDBMain.GetConMain);
                                    try
                                    {
                                        SqlDataReader Libpdr = Libpcom.ExecuteReader();

                                        while (Libpdr.Read())
                                        {
                                            // ----- Create 'New Char ID'
                                            NewLibp_ID = NumericsCount.ToString() + Libpdr["Libp_ID"].ToString() + "_" + NewChp;

                                            // ----- Create 'New Char Password'
                                            NewCharPass = Libpdr["Libp_CharPassword"].ToString() + NewCharValue;

                                            // ----- Create 'New Char Password Hashed'
                                            NewCharPassHash = GetStrToMD5(NewCharPass);

                                            #region Insert 'New Char Pass-Hashed'
                                            //----------

                                            string Q_LibpInsert = "INSERT INTO " + tblNameLibraryPass + NumericsCount + "(Libp_ID, Libp_CharPassword, Libp_CharPasswordHashed)" +
                                                                                                                 " VALUES(@Libp_ID, @Libp_CharPassword, @Libp_CharPasswordHashed);";
                                            SqlCommand LibpInsertcom = new SqlCommand(Q_LibpInsert, _ConDBMain.GetConMain);

                                            LibpInsertcom.Parameters.AddWithValue("@Libp_ID", NewLibp_ID);
                                            LibpInsertcom.Parameters.AddWithValue("@Libp_CharPassword", NewCharPass);
                                            LibpInsertcom.Parameters.AddWithValue("@Libp_CharPasswordHashed", NewCharPassHash);

                                            try
                                            {
                                                LibpInsertcom.ExecuteNonQuery();
                                            }
                                            catch (Exception exp)
                                            {
                                                SettingsMain.Default.CounterErrorChf_int++;
                                                noError = false;

                                                MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۱۲۴: " + exp.Message;

                                                if (!_TryCatch.GetShowFriendlyMessage)
                                                {
                                                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۱۲۴: " + exp.Message;
                                                }
                                                else
                                                {
                                                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۱۲۴: " + "خطا در ثبت اطلاعات کلمه عبور" + NumericsCount + " رقمی " +
                                                        "( ID='" + NewLibp_ID + "' Password='" + NewCharPass + "' Password_Hashed='" + NewCharPassHash + "' ).   ";
                                                }
                                            }

                                            //----------
                                            #endregion Insert 'New Char Pass-Hashed'

                                            SettingsMain.Default.CounterChf_int++;
                                        }
                                        Libpdr.Close();
                                    }
                                    catch (Exception exp)
                                    {
                                        noError = false;

                                        MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۱۱۳: " + exp.Message;

                                        if (!_TryCatch.GetShowFriendlyMessage)
                                        {
                                            MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۱۱۳: " + exp.Message;
                                        }
                                        else
                                        {
                                            MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۱۱۳: " + "خطا در بارگیری اطلاعات کلمات عبور" + NumericsCount + " رقمی.   ";
                                        }
                                    }

                                    //----------
                                    #endregion Load 'Before Numerics Count'
                                }
                            }
                            Chfdr.Close();
                        }
                        catch (Exception exp)
                        {
                            noError = false;

                            MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۱۱۲: " + exp.Message;

                            if (!_TryCatch.GetShowFriendlyMessage)
                            {
                                MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۱۱۲: " + exp.Message;
                            }
                            else
                            {
                                MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۱۱۲: " + "خطا در بارگیری اطلاعات کلمات عبور.   ";
                            }
                        }
                    }
                    catch { noError = false; }
                }

                //----------
                #endregion Load 'New Numerics Count'

                _ConDBMain.GetConMain.Close();
            }
            catch (Exception exp)
            {
                noError = false;

                MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۱۰۰: " + exp.Message;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۱۰۰: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۱۰۰: " + _TryCatch.GetMEC_ConDB;
                }

                try
                {
                    _ConDBMain.GetConMain.Close();
                }
                catch { }
            }

            try
            {
                SettingsMain.Default.CEM_ErrorCreatePPLib_NotFriendly += MesExpErrorForms_NotFriendly;
                SettingsMain.Default.CEM_ErrorCreatePPLib += MesExpErrorForms;
            }
            catch { }

            return noError;
        }

        public bool CreateSHA1Library(int GetNumericsCount)
        {
            bool noError = true;
            tblNameLibraryPass = "SHA1LibraryPass_";

            MesExpErrorForms = "";

            try
            {
                _ConDBMain.GetConMain.Open();

                #region Load 'New Numerics Count'
                //----------

                for (NumericsCount = 1; NumericsCount <= GetNumericsCount; NumericsCount++)
                {
                    BeforeNumericsCount = NumericsCount - 1;
                    string Q_LibpLoad = "SELECT * FROM " + tblNameLibraryPass + BeforeNumericsCount +
                                       " ORDER by Libp_ID ASC;";

                    try
                    {
                        #region Create 'New tblLibraryPass_NumericsCount'
                        //----------

                        string Q_tblLibPassCreate = CNewTableLibPass_NCount(tblNameLibraryPass, NumericsCount);

                        SqlCommand tblLibPCcom = new SqlCommand(Q_tblLibPassCreate, _ConDBMain.GetConMain);

                        try
                        {
                            tblLibPCcom.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            noError = false;

                            MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۲۵۱: " + exp.Message;

                            if (!_TryCatch.GetShowFriendlyMessage)
                            {
                                MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۲۵۱: " + exp.Message;
                            }
                            else
                            {
                                MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۲۵۱: " + "خطا در ایجاد جدول کلمات عبور " + NumericsCount + " رقمی.   ";
                            }
                        }

                        //----------
                        #endregion Create 'New tblLibraryPass_NumericsCount'

                        SqlCommand Chfcom = new SqlCommand(Q_ChfLoad, _ConDBMain.GetConMain);
                        try
                        {
                            SqlDataReader Chfdr = Chfcom.ExecuteReader();

                            while (Chfdr.Read())
                            {
                                Chf_intStart = Convert.ToInt32(Chfdr["Chf_intStart"].ToString());
                                Chf_intEnd = Convert.ToInt32(Chfdr["Chf_intEnd"].ToString());

                                NewCharValue = string.Empty;

                                for (int NewChp = Chf_intStart; NewChp <= Chf_intEnd; NewChp++)
                                {
                                    // ----- Create 'New Numeric Count'
                                    NewCharValue = Convert.ToChar(NewChp).ToString();

                                    #region Load 'Before Numerics Count'
                                    //----------

                                    SqlCommand Libpcom = new SqlCommand(Q_LibpLoad, _ConDBMain.GetConMain);
                                    try
                                    {
                                        SqlDataReader Libpdr = Libpcom.ExecuteReader();

                                        while (Libpdr.Read())
                                        {
                                            // ----- Create 'New Char ID'
                                            NewLibp_ID = NumericsCount.ToString() + Libpdr["Libp_ID"].ToString() + "_" + NewChp;

                                            // ----- Create 'New Char Password'
                                            NewCharPass = Libpdr["Libp_CharPassword"].ToString() + NewCharValue;

                                            // ----- Create 'New Char Password Hashed'
                                            NewCharPassHash = GetStrToSHA1(NewCharPass);

                                            #region Insert 'New Char Pass-Hashed'
                                            //----------

                                            string Q_LibpInsert = "INSERT INTO " + tblNameLibraryPass + NumericsCount + "(Libp_ID, Libp_CharPassword, Libp_CharPasswordHashed)" +
                                                                                                                 " VALUES(@Libp_ID, @Libp_CharPassword, @Libp_CharPasswordHashed);";
                                            SqlCommand LibpInsertcom = new SqlCommand(Q_LibpInsert, _ConDBMain.GetConMain);

                                            LibpInsertcom.Parameters.AddWithValue("@Libp_ID", NewLibp_ID);
                                            LibpInsertcom.Parameters.AddWithValue("@Libp_CharPassword", NewCharPass);
                                            LibpInsertcom.Parameters.AddWithValue("@Libp_CharPasswordHashed", NewCharPassHash);

                                            try
                                            {
                                                LibpInsertcom.ExecuteNonQuery();
                                            }
                                            catch (Exception exp)
                                            {
                                                SettingsMain.Default.CounterErrorChf_int++;
                                                noError = false;

                                                MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۲۲۴: " + exp.Message;

                                                if (!_TryCatch.GetShowFriendlyMessage)
                                                {
                                                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۲۲۴: " + exp.Message;
                                                }
                                                else
                                                {
                                                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۲۲۴: " + "خطا در ثبت اطلاعات کلمه عبور" + NumericsCount + " رقمی " +
                                                        "( ID='" + NewLibp_ID + "' Password='" + NewCharPass + "' Password_Hashed='" + NewCharPassHash + "' ).   ";
                                                }
                                            }

                                            //----------
                                            #endregion Insert 'New Char Pass-Hashed'

                                            SettingsMain.Default.CounterChf_int++;
                                        }
                                        Libpdr.Close();
                                    }
                                    catch (Exception exp)
                                    {
                                        noError = false;

                                        MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۲۱۳: " + exp.Message;

                                        if (!_TryCatch.GetShowFriendlyMessage)
                                        {
                                            MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۲۱۳: " + exp.Message;
                                        }
                                        else
                                        {
                                            MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۲۱۳: " + "خطا در بارگیری اطلاعات کلمات عبور" + NumericsCount + " رقمی.   ";
                                        }
                                    }

                                    //----------
                                    #endregion Load 'Before Numerics Count'
                                }
                            }
                            Chfdr.Close();
                        }
                        catch (Exception exp)
                        {
                            noError = false;

                            MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۲۱۲: " + exp.Message;

                            if (!_TryCatch.GetShowFriendlyMessage)
                            {
                                MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۲۱۲: " + exp.Message;
                            }
                            else
                            {
                                MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۲۱۲: " + "خطا در بارگیری اطلاعات کلمات عبور.   ";
                            }
                        }
                    }
                    catch { noError = false; }
                }

                //----------
                #endregion Load 'New Numerics Count'

                _ConDBMain.GetConMain.Close();
            }
            catch (Exception exp)
            {
                noError = false;

                MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۲۰۰: " + exp.Message;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۲۰۰: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۲۰۰: " + _TryCatch.GetMEC_ConDB;
                }

                try
                {
                    _ConDBMain.GetConMain.Close();
                }
                catch { }
            }

            try
            {
                SettingsMain.Default.CEM_ErrorSearchPPLib_NotFriendly += MesExpErrorForms_NotFriendly;
                SettingsMain.Default.CEM_ErrorSearchPPLib += MesExpErrorForms;
            }
            catch { }

            return noError;
        }

        //----------
        #endregion Create Protected Algoritms

        #region Search Protected Algoritms
        //----------

        public string GetSearchSumTblLib_int(int tblLibPassCount, int NCount)
        {
            string MesExpErrorForms = string.Empty;

            SettingsMain.Default.SumTblLib_int = 0;

            try
            {
                SettingsMain.Default.SumTblLib_int = tblLibPassCount * NCount;
            }
            catch (Exception exp)
            {
                MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۲۰۰۰: " + exp.Message;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۲۰۰۰: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۲۰۰۰: " + "خطا در بارگیری اطلاعات جدولات کلمات عبور.   ";
                }
            }

            return MesExpErrorForms;
        }

        public string SearchMD5Library(string strPassHashed, int GetSrchNumericsCount)
        {
            string strMainPass = string.Empty;
            tblNameLibraryPass = "MD5LibraryPass_";

            MesExpErrorForms = "";

            try
            {
                _ConDBMain.GetConMain.Open();

                #region Load 'New Numerics Count'
                //----------

                for (NumericsCount = 1; NumericsCount <= GetSrchNumericsCount; NumericsCount++)
                {
                    string Q_PPLibLoad = "SELECT * FROM " + tblNameLibraryPass + NumericsCount +
                                        " WHERE Libp_CharPasswordHashed='" + strPassHashed + "'" +
                                        " ORDER by Libp_ID ASC;";

                    SqlCommand PPLibcom = new SqlCommand(Q_PPLibLoad, _ConDBMain.GetConMain);

                    try
                    {
                        SqlDataReader PPLibdr = PPLibcom.ExecuteReader();

                        if (PPLibdr.Read())
                        {
                            strMainPass = PPLibdr["Libp_CharPassword"].ToString();

                            PPLibdr.Close();

                            SettingsMain.Default.CounterTblLib_int++;
                            SettingsMain.Default.Find_BreakSearchPPLib = true;
                            break;
                        }
                        PPLibdr.Close();
                    }
                    catch (Exception exp)
                    {
                        SettingsMain.Default.CounterErrorTblLib_int++;

                        MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۳۱۱: " + exp.Message;

                        if (!_TryCatch.GetShowFriendlyMessage)
                        {
                            MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۳۱۱: " + exp.Message;
                        }
                        else
                        {
                            MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۳۱۱: " + "خطا در بارگیری اطلاعات کلمات عبور" + NumericsCount + " رقمی.   ";
                        }
                    }

                    SettingsMain.Default.CounterTblLib_int++;
                }

                //----------
                #endregion Load 'New Numerics Count'

                _ConDBMain.GetConMain.Close();
            }
            catch (Exception exp)
            {
                MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۳۰۰: " + exp.Message;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۳۰۰: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۳۰۰: " + _TryCatch.GetMEC_ConDB;
                }

                try
                {
                    _ConDBMain.GetConMain.Close();
                }
                catch { }
            }

            try
            {
                SettingsMain.Default.CEM_ErrorSearchPPLib_NotFriendly += MesExpErrorForms_NotFriendly;
                SettingsMain.Default.CEM_ErrorSearchPPLib += MesExpErrorForms;
            }
            catch { }

            return strMainPass;
        }

        public string SearchSHA1Library(string strPassHashed, int GetSrchNumericsCount)
        {
            string strMainPass = string.Empty;
            tblNameLibraryPass = "SHA1LibraryPass_";

            MesExpErrorForms = "";

            try
            {
                _ConDBMain.GetConMain.Open();

                #region Load 'New Numerics Count'
                //----------

                for (NumericsCount = 1; NumericsCount <= GetSrchNumericsCount; NumericsCount++)
                {
                    string Q_PPLibLoad = "SELECT * FROM " + tblNameLibraryPass + NumericsCount +
                                        " WHERE Libp_CharPasswordHashed='" + strPassHashed + "'" +
                                        " ORDER by Libp_ID ASC;";

                    SqlCommand PPLibcom = new SqlCommand(Q_PPLibLoad, _ConDBMain.GetConMain);

                    try
                    {
                        SqlDataReader PPLibdr = PPLibcom.ExecuteReader();

                        if (PPLibdr.Read())
                        {
                            strMainPass = PPLibdr["Libp_CharPassword"].ToString();

                            PPLibdr.Close();

                            SettingsMain.Default.Find_BreakSearchPPLib = true;
                            break;
                        }
                        PPLibdr.Close();
                    }
                    catch (Exception exp)
                    {
                        SettingsMain.Default.CounterErrorTblLib_int++;

                        MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۴۱۱: " + exp.Message;

                        if (!_TryCatch.GetShowFriendlyMessage)
                        {
                            MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۴۱۱: " + exp.Message;
                        }
                        else
                        {
                            MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۴۱۱: " + "خطا در بارگیری اطلاعات کلمات عبور" + NumericsCount + " رقمی.   ";
                        }
                    }

                    SettingsMain.Default.CounterTblLib_int++;
                }

                //----------
                #endregion Load 'New Numerics Count'

                _ConDBMain.GetConMain.Close();
            }
            catch (Exception exp)
            {
                MesExpErrorForms_NotFriendly += _TryCatch.GetMEC_McsPPLib + "۰۴۰۰: " + exp.Message;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۴۰۰: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsPPLib + "۰۴۰۰: " + _TryCatch.GetMEC_ConDB;
                }

                try
                {
                    _ConDBMain.GetConMain.Close();
                }
                catch { }
            }

            try
            {
                SettingsMain.Default.CEM_ErrorSearchPPLib_NotFriendly += MesExpErrorForms_NotFriendly;
                SettingsMain.Default.CEM_ErrorSearchPPLib += MesExpErrorForms;
            }
            catch { }

            return strMainPass;
        }

        //----------
        #endregion Search Protected Algoritms

    }
}