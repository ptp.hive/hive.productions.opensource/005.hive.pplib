﻿using System;
//
using ECP.PersianMessageBox;

namespace PPLib_hIVE.Models_CS
{
    class TryCatch : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region TryCatch
        //----------

        /// <summary>
        /// Algoritm ShowMessageError
        /// </summary>
        /// <param name="MessageError"></param>
        /// <returns></returns>
        /*

        string ShowMessageError = "a-bc-d-e";
        
            'a' is for specification Forms Program
                0 = FrmStartup
                1 = FrmSearch
                2 = FrmInfo
         
            'bc' is for Agency-Number [errors] each Function
        
            'd' is for specification Querys:
                0 = noQuery
                1 = SELECT
                2 = INSERT
                3 = UPDATE
                4 = DELETE
                5 = CREATE
                
            'e' is for Agency-Number errors Sections-Function
                first Section is '0'
        
        */

        /// <summary>
        /// Message Error Code Static
        /// </summary>
        private bool ShowFriendlyMessage = true;
        private static string MEC_ConnectionDB = "خطا در برقراری ارتباط با پایگاه‌داده.   ";
        private static string MessageExpErrorSorry = "\n\n-متاسفیم.   \n";
        private static string MesErrCodeForm = "\n-خطای ";

        /// <summary>
        /// Message Error Code Forms
        /// </summary>
        private static string MEC_FrmStartup = "۰";
        private static string MEC_FrmSearch = "۱";
        private static string MEC_FrmInfo = "۲";

        /// <summary>
        /// Message Error Code Models CS
        /// </summary>
        private static string MEC_McsPPLib = "۱۱";

        //----------

        private bool CatchExceptionMesError(string MessageError)
        {
            bool isError = false;

            try
            {
                if (MessageError != "")
                {
                    isError = true;

                    MessageError += MessageExpErrorSorry;

                    PersianMessageBox.Show(MessageError,
                                           "خطا",
                                           PersianMessageBox.Buttons.OK,
                                           PersianMessageBox.Icon.Error,
                                           PersianMessageBox.DefaultButton.Button1);

                    MessageError = "";
                }
            }
            catch { }

            return isError;
        }

        //----------
        #endregion TryCatch

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        public bool GetShowFriendlyMessage
        {
            get { return ShowFriendlyMessage; }
        }

        public bool GetCEM_Error(string MessageError)
        {
            return CatchExceptionMesError(MessageError);
        }

        public string GetMEC_ConDB
        {
            get { return MEC_ConnectionDB; }
        }

        #region TryCatch Message Error Code Forms
        //----------

        public string GetMEC_FrmStartup
        {
            get { return MesErrCodeForm + MEC_FrmStartup; }
        }
        public string GetMEC_FrmSearch
        {
            get { return MesErrCodeForm + MEC_FrmSearch; }
        }
        public string GetMEC_FrmInfo
        {
            get { return MesErrCodeForm + MEC_FrmInfo; }
        }

        //----------
        #endregion TryCatch Message Error Code Forms

        #region TryCatch Message Error Code Models CS
        //----------

        public string GetMEC_McsPPLib
        {
            get { return MesErrCodeForm + MEC_McsPPLib; }
        }

        //----------
        #endregion TryCatch Message Error Code Models CS

    }
}