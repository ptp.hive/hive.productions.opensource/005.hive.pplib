﻿using System;
//
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PPLib_hIVE.Models_CS
{
    class ConnectionDBMain : IDisposable
    {

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region ConnectionDataBaseMain
        //----------

        private static string DbName = "PPLibDB-Exp.mdf";
        private static string DbNameNoType = "PPLibDB-Exp";

        // For Test ...
        private static string DSStartupPathG = "G:\\DataBase";
        private static string DSStartupPath = Application.StartupPath + "\\DataBase";
        private static string DbNoPassword = "";

        //

        private static string StrDataSourceConExp = "Data Source=.\\SQLExpress;";
        private static string StrDataSourceConEnt = "Server=(LocalDB)\\v10.0;";

        //

        private static string MainStrConnectionData = StrDataSourceConExp +
                                                      "Initial Catalog=" + DbNameNoType + "; " +
                                                      "Integrated Security=True; " +
                                                      "MultipleActiveResultSets=True; " +
                                                      "Pooling=False;";

        private static string MainStrConnection = "Server=(Local); " +
                                                  "Database=" + DbNameNoType + "; " +
                                                  "Integrated Security=True; " +
                                                  "MultipleActiveResultSets=True; " +
                                                  "Pooling=False;";

        private static string MainStrConnectionLDB = StrDataSourceConExp +
                                                     "AttachDbFileName=" + DSStartupPath +
                                                     "\\" + DbName + "; " +
                                                     "Integrated Security=True; " +
                                                     "MultipleActiveResultSets=True; " +
                                                     "Pooling=False;";

        private static SqlConnection MainConnection = new SqlConnection(MainStrConnectionData);

        //----------
        #endregion ConnectionDataBaseMain

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        public string GetStrConMain
        {
            get { return MainStrConnection; }
        }

        public SqlConnection GetConMain
        {
            get { return MainConnection; }
        }
    }
}