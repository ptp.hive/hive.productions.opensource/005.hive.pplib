﻿using System;
using System.Windows.Forms;
//
using ECP.PersianMessageBox;
using PPLib_hIVE.Models_CS;
using System.ComponentModel;
using System.Threading;

namespace PPLib_hIVE
{
    public partial class FrmSearch : Form
    {
        public FrmSearch()
        {
            InitializeComponent();
        }

        #region ConnectionModels
        //----------

        private ConnectionDBMain _ConDBMain = new ConnectionDBMain();
        private ProtectedPassLibrary _PPLib = new ProtectedPassLibrary();

        //----------
        #endregion ConnectionModels

        #region MultiThreaded
        //----------

        private DateTime dtStartProcess = DateTime.Now;
        private TimeSpan tsEndToStart;
        //
        private Thread trSearchPPLib;
        private ThreadStart trsSearchPPLib;
        //
        private Thread trTimer;
        private ThreadStart trsTimer;

        //----------
        #endregion MultiThreaded

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        #region Form Main
        //----------

        private void SearchForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (PersianMessageBox.Show("آیا می‌خواهید از صفحه جستجو خارج شوید؟   \n",
                                       "جستجو",
                                       PersianMessageBox.Buttons.YesNo,
                                       PersianMessageBox.Icon.Question,
                                       PersianMessageBox.DefaultButton.Button2) != DialogResult.Yes)
            {
                e.Cancel = true;
            }
            else
            {
                try
                {
                    bgwSearchPPLib.CancelAsync();
                }
                catch { }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //----------
        #endregion Form Main

        #region Detail Password
        //----------

        private void btnPassHashedPaste_Click(object sender, EventArgs e)
        {
            try
            {
                Invoke(new MethodInvoker(delegate () { txtPasswordHashed.Text = Clipboard.GetText(); }));
            }
            catch { }
        }

        private void btnPassHashedDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Invoke(new MethodInvoker(delegate () { txtPasswordHashed.Text = ""; }));
            }
            catch { }
        }

        private void btnPassCopy_Click(object sender, EventArgs e)
        {
            try
            {
                Invoke(new MethodInvoker(delegate () { Clipboard.SetText(txtPassword.Text); }));

                PersianMessageBox.Show("ایجاد شد.   \n",
                                       "رمزعبور",
                                       PersianMessageBox.Buttons.OK,
                                       PersianMessageBox.Icon.Information,
                                       PersianMessageBox.DefaultButton.Button1);
            }
            catch { }
        }

        //----------
        #endregion Detail Password

        #region Timer Status
        //----------

        private double iPersent = -1;
        private double iePersent = -1;

        private void StartTimerStatus()
        {
            try
            {
                ProgressBarStatusPersent.Value = 0;
                lblStatusPersentProcces.Text = "0 % Procces";
                lblStatusPersentTrueProcces.Text = "0 % True Procces";
                //
                dtStartProcess = DateTime.Now;
                tsEndToStart = dtStartProcess.Subtract(dtStartProcess);
                Invoke(new MethodInvoker(delegate () { lblTimerStatusLasted.Text = tsEndToStart.ToString(); }));

                timerSearchPPLib.Enabled = true;
            }
            catch { }
        }

        private void TimerStatusCounter()
        {
            try
            {
                tsEndToStart = DateTime.Now.Subtract(dtStartProcess);
                Invoke(new MethodInvoker(delegate () { lblTimerStatusLasted.Text = tsEndToStart.ToString(); }));
                //
                iPersent = ((SettingsMain.Default.CounterTblLib_int / SettingsMain.Default.SumTblLib_int) * 100);
                iPersent = (Math.Round(iPersent * (int)Math.Pow(10, SettingsMain.Default.xPow) / (int)Math.Pow(10, SettingsMain.Default.xPow)));
                Invoke(new MethodInvoker(delegate () { ProgressBarStatusPersent.Value = Convert.ToInt32(iPersent); }));
                Invoke(new MethodInvoker(delegate () { lblStatusPersentProcces.Text = iPersent + " % Procces"; }));
                //
                iePersent = (SettingsMain.Default.CounterTblLib_int - SettingsMain.Default.CounterErrorTblLib_int) / SettingsMain.Default.SumTblLib_int * 100;
                iePersent = (Math.Round(iePersent * (int)Math.Pow(10, SettingsMain.Default.xPow) / (int)Math.Pow(10, SettingsMain.Default.xPow)));
                Invoke(new MethodInvoker(delegate () { lblStatusPersentTrueProcces.Text = iePersent + " % True Procces"; }));
            }
            catch { }
        }

        private void timerSearchPPLib_Tick(object sender, EventArgs e)
        {
            try
            {
                trsTimer = new ThreadStart(TimerStatusCounter);
                trTimer = new Thread(trsTimer);
                trTimer.Priority = ThreadPriority.AboveNormal;
                //
                trTimer.Start();
            }
            catch { }
        }

        private void StopTimerStatus()
        {
            try
            {
                timerSearchPPLib.Enabled = false;
                //
                tsEndToStart = DateTime.Now.Subtract(dtStartProcess);
                Invoke(new MethodInvoker(delegate () { lblTimerStatusLasted.Text = tsEndToStart.ToString(); }));
                Invoke(new MethodInvoker(delegate () { lblStatusPersentProcces.Text = "100 % Procces"; }));
                //
                Invoke(new MethodInvoker(delegate () { ProgressBarStatusPersent.Value = 100; }));

                if (SettingsMain.Default.Find_BreakSearchPPLib)
                {
                    iePersent = (SettingsMain.Default.CounterTblLib_int - SettingsMain.Default.CounterErrorTblLib_int) / SettingsMain.Default.CounterTblLib_int * 100;
                    iePersent = (Math.Round(iePersent * (int)Math.Pow(10, SettingsMain.Default.xPow) / (int)Math.Pow(10, SettingsMain.Default.xPow)));
                    Invoke(new MethodInvoker(delegate () { lblStatusPersentTrueProcces.Text = iePersent + " % True Procces"; }));
                }
                else
                {
                    iePersent = (SettingsMain.Default.CounterTblLib_int - SettingsMain.Default.CounterErrorTblLib_int) / SettingsMain.Default.SumTblLib_int * 100;
                    iePersent = (Math.Round(iePersent * (int)Math.Pow(10, SettingsMain.Default.xPow) / (int)Math.Pow(10, SettingsMain.Default.xPow)));
                    Invoke(new MethodInvoker(delegate () { lblStatusPersentTrueProcces.Text = iePersent + " % True Procces"; }));
                }
            }
            catch { }
        }

        //----------
        #endregion Timer Status

        #region Search New PPLibrary
        //----------

        private void bgwSearchPPLib_DoWork(object sender, DoWorkEventArgs e)
        {
            bool isErrorSPP = true;
            Invoke(new MethodInvoker(delegate () { isErrorSPP = isErrorSearchPPLibrary(); }));

            if (!isErrorSPP)
            {
                try
                {
                    if (PersianMessageBox.Show("آیا می‌خواهید « کلمه عبور »‌ جدیدی جستجو کنید؟   ",
                                               "جستجو کلمه عبور جدید",
                                               PersianMessageBox.Buttons.YesNo,
                                               PersianMessageBox.Icon.Question,
                                               PersianMessageBox.DefaultButton.Button1) == DialogResult.Yes)
                    {
                        int SelPAlgoritm = -1;
                        Invoke(new MethodInvoker(delegate () { SelPAlgoritm = cmbSrchSelectedProtectedAlgoritm.SelectedIndex; }));

                        int tblLibPassCount = 0;

                        if (SelPAlgoritm == 0)
                        {
                            tblLibPassCount = cmbSrchSelectedProtectedAlgoritm.Items.Count - 1;
                        }
                        else
                        {
                            tblLibPassCount = 1;
                        }

                        string ErrorGetSumTblLib_int = _PPLib.GetSearchSumTblLib_int(tblLibPassCount, Int32.Parse(numUDSrchNumericsCount.Value.ToString()));

                        if (ErrorGetSumTblLib_int == "")
                        {
                            if (PersianMessageBox.Show("آیا می‌خواهید کلمه عبور جدید « تا " +
                                                   numUDSrchNumericsCount.Value + " رقم » جستجو کنید؟   ",
                                                   "جستجو کلمه عبور",
                                                   PersianMessageBox.Buttons.YesNo,
                                                   PersianMessageBox.Icon.Question,
                                                   PersianMessageBox.DefaultButton.Button1) == DialogResult.Yes)
                            {
                                Invoke(new MethodInvoker(delegate ()
                                {
                                    tlpSearchLibPass.Enabled = false;
                                    tlpDetailPass.Enabled = false;
                                }));

                                SettingsMain.Default.CEM_ErrorSearchPPLib = "";
                                SettingsMain.Default.CounterTblLib_int = 0;
                                SettingsMain.Default.CounterErrorTblLib_int = 0;
                                SettingsMain.Default.Find_BreakSearchPPLib = false;

                                Invoke(new MethodInvoker(delegate () { StartTimerStatus(); }));

                                int cmbSrchSelPACount = cmbSrchSelectedProtectedAlgoritm.Items.Count;
                                string[] strMainPass = new string[cmbSrchSelPACount];
                                string strMainPassword = string.Empty;

                                for (int i = 0; i < cmbSrchSelPACount; i++)
                                {
                                    strMainPass[i] = string.Empty;
                                }

                                SettingsMain.Default.CEM_ErrorSearchPPLib = "";

                                #region cmb Search Selected Protected Algoritm
                                //----------

                                if (SelPAlgoritm == 0)
                                {
                                    strMainPass[1] = _PPLib.SearchMD5Library(txtPasswordHashed.Text, Int32.Parse(numUDSrchNumericsCount.Value.ToString()));
                                    if (strMainPass[1] != null) { strMainPass[0] = strMainPass[1]; }
                                    //
                                    else
                                    {
                                        strMainPass[2] = _PPLib.SearchSHA1Library(txtPasswordHashed.Text, Int32.Parse(numUDSrchNumericsCount.Value.ToString()));
                                        if (strMainPass[2] != null) { strMainPass[0] = strMainPass[2]; }
                                    }

                                    strMainPassword = strMainPass[0];
                                }

                                else if (SelPAlgoritm == 1)
                                {
                                    strMainPass[1] = _PPLib.SearchMD5Library(txtPasswordHashed.Text, Int32.Parse(numUDSrchNumericsCount.Value.ToString()));

                                    strMainPassword = strMainPass[1];
                                }

                                else if (SelPAlgoritm == 2)
                                {
                                    strMainPass[2] = _PPLib.SearchSHA1Library(txtPasswordHashed.Text, Int32.Parse(numUDSrchNumericsCount.Value.ToString()));

                                    strMainPassword = strMainPass[2];
                                }

                                //----------
                                #endregion cmb Search Selected Protected Algoritm

                                Invoke(new MethodInvoker(delegate () { txtPassword.Text = strMainPassword; }));

                                Invoke(new MethodInvoker(delegate () { StopTimerStatus(); }));

                                Invoke(new MethodInvoker(delegate ()
                                {
                                    tlpSearchLibPass.Enabled = true;
                                    tlpDetailPass.Enabled = true;
                                }));

                                if (strMainPassword != "")
                                {
                                    PersianMessageBox.Show("کلمه عبور با موفقیت یافت شد.   \n",
                                                           "جستجو کلمه عبور",
                                                           PersianMessageBox.Buttons.OK,
                                                           PersianMessageBox.Icon.Information,
                                                           PersianMessageBox.DefaultButton.Button1);
                                }
                                else
                                {
                                    PersianMessageBox.Show("کلمه عبور یافت نشد.   \n",
                                                           "جستجو کلمه عبور",
                                                           PersianMessageBox.Buttons.OK,
                                                           PersianMessageBox.Icon.Information,
                                                           PersianMessageBox.DefaultButton.Button1);
                                }

                                if (SettingsMain.Default.CEM_ErrorSearchPPLib != "")
                                {
                                    if (PersianMessageBox.Show("جستجوی « کلمه عبور »‌ با  خطا همراه بود.      \n" +
                                                              "\n مایل به مشاهده جزئیات خطا هستید؟ ",
                                                              "خطا",
                                                              PersianMessageBox.Buttons.YesNo,
                                                              PersianMessageBox.Icon.Error,
                                                              PersianMessageBox.DefaultButton.Button1) == DialogResult.Yes)
                                    {
                                        try
                                        {
                                            SettingsMain.Default.CEM_MesErrorsForm_NotFriendly = SettingsMain.Default.CEM_ErrorSearchPPLib_NotFriendly;
                                            SettingsMain.Default.CEM_MesErrorsForm = SettingsMain.Default.CEM_ErrorSearchPPLib;

                                            FrmErrors _FormErrors = new FrmErrors();
                                            _FormErrors.ShowDialog();
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                        else
                        {
                            PersianMessageBox.Show(ErrorGetSumTblLib_int,
                                                   "خطا",
                                                   PersianMessageBox.Buttons.OK,
                                                   PersianMessageBox.Icon.Error,
                                                   PersianMessageBox.DefaultButton.Button1);
                        }
                    }
                }
                catch { }
            }
            else
            {
                PersianMessageBox.Show(SettingsMain.Default.MessageSearchPPLibError,
                                       "خطا",
                                       PersianMessageBox.Buttons.OK,
                                       PersianMessageBox.Icon.Error,
                                       PersianMessageBox.DefaultButton.Button1);
            }
        }

        private bool isErrorSearchPPLibrary()
        {
            bool isError = false;
            SettingsMain.Default.MessageSearchPPLibError = "قسمت‌های زیر را تکمیل کنید:      \n";

            try
            {
                if (txtPasswordHashed.Text == "")
                {
                    SettingsMain.Default.MessageSearchPPLibError += "\n رمزعبور رمزنگاری شده را وارد کنید. ";
                    isError = true;
                }
                if (cmbSrchSelectedProtectedAlgoritm.SelectedItem == null)
                {
                    SettingsMain.Default.MessageSearchPPLibError += "\n الگوریتم رمزنگاری را انتخاب کنید. ";
                    isError = true;
                }
            }
            catch { isError = true; }

            return isError;
        }

        private void btnSearchLibrary_Click(object sender, EventArgs e)
        {
            try
            {
                trsSearchPPLib = new ThreadStart(bgwSearchPPLib.RunWorkerAsync);
                trSearchPPLib = new Thread(trsSearchPPLib);
                trSearchPPLib.Priority = ThreadPriority.Normal;
                //
                trSearchPPLib.Start();
            }
            catch { }
        }

        //----------
        #endregion Search New PPLibrary

        //------------
        ////----------////---------------------------------------------------------------------// Begin FormSearch_Load
        //------------

        private void SearchForm_Load(object sender, EventArgs e)
        {
            string MessageError = string.Empty;

            try
            {
                this.TopMost = true;
                this.TopMost = false;
            }
            catch
            {
                MessageError = "\nفرآیند بارگیری برنامه با خطا همراه بود.   \n" +
                               "\nمتاسفیم.   \n";

                PersianMessageBox.Show(MessageError,
                                   "خطا",
                                   PersianMessageBox.Buttons.OK,
                                   PersianMessageBox.Icon.Error,
                                   PersianMessageBox.DefaultButton.Button1);
            }
        }

        //------------
        ////----------////---------------------------------------------------------------------// End FormSearch_Load
        //------------
    }
}