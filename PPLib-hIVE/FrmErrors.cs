﻿using System;
using System.Windows.Forms;
//
using ECP.PersianMessageBox;

namespace PPLib_hIVE
{
    public partial class FrmErrors : Form
    {
        public FrmErrors()
        {
            InitializeComponent();
        }

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        private void btnErrorsSave_Click(object sender, EventArgs e)
        {

        }

        //------------
        ////----------////---------------------------------------------------------------------// Begin FormErrors_Load
        //------------

        private void FrmErrors_Load(object sender, EventArgs e)
        {
            string MessageError = string.Empty;

            try
            {
                rtbCEM_MesErrorsForm_Friendly.Text = SettingsMain.Default.CEM_MesErrorsForm;
                rtbCEM_MesErrorsForm_NotFriendly.Text = SettingsMain.Default.CEM_MesErrorsForm_NotFriendly;
            }
            catch
            {
                MessageError = "\nفرآیند بارگیری پنجره خطاها، با خطا همراه بود.   \n" +
                               "\nمتاسفیم.   \n";

                PersianMessageBox.Show(MessageError,
                                   "خطا",
                                   PersianMessageBox.Buttons.OK,
                                   PersianMessageBox.Icon.Error,
                                   PersianMessageBox.DefaultButton.Button1);
            }
        }

        //------------
        ////----------////---------------------------------------------------------------------// End FormErrors_Load
        //------------

    }
}