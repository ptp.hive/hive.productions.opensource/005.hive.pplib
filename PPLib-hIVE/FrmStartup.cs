﻿using System;
using System.Drawing;
using System.Windows.Forms;
//
using ECP.PersianMessageBox;
using PPLib_hIVE.Models_CS;
using System.Threading;

namespace PPLib_hIVE
{
    public partial class FrmStartup : Form
    {
        public FrmStartup()
        {
            InitializeComponent();
        }

        #region ConnectionModels
        //----------

        private ProtectedPassLibrary _PPLib = new ProtectedPassLibrary();

        private TryCatch _TryCatch = new TryCatch();
        private string MesExpErrorForms = string.Empty;

        //----------
        #endregion ConnectionModels

        #region MultiThreaded
        //----------

        private DateTime dtStartProcess = DateTime.Now;
        private TimeSpan tsEndToStart;
        //
        private Thread trCreatePPLib;
        private ThreadStart trsCreatePPLib;
        //
        private Thread trSearchLibrary;
        private ThreadStart trsSearchLibrary;
        //
        private Thread trTimer;
        private ThreadStart trsTimer;

        //----------
        #endregion MultiThreaded

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        #region Form Main
        //----------

        #region FormDragwithMouse

        private void frmDragForm_Paint(object sender, PaintEventArgs e)
        {
            //Draws a border to make the Form stand out
            //Just done for appearance, not necessary

            Pen p = new Pen(Color.Gray, 3);
            e.Graphics.DrawRectangle(p, 0, 0, this.Width - 1, this.Height - 1);
            p.Dispose();
        }

        Point lastClick; //Holds where the Form was clicked

        private void frmDragForm_MouseDown(object sender, MouseEventArgs e)
        {
            lastClick = new Point(e.X, e.Y); //We'll need this for when the Form starts to move
        }

        private void frmDragForm_MouseMove(object sender, MouseEventArgs e)
        {
            //Point newLocation = new Point(e.X - lastE.X, e.Y - lastE.Y);
            if (e.Button == MouseButtons.Left) //Only when mouse is clicked
            {
                //Move the Form the same difference the mouse cursor moved;
                this.Left += e.X - lastClick.X;
                this.Top += e.Y - lastClick.Y;
            }
        }

        //If the user clicks on the Objects(on the form) we want the same kind of behavior
        //so we just call the Forms corresponding methods
        private void frmDragObjects_MouseDown(object sender, MouseEventArgs e)
        {
            frmDragForm_MouseDown(sender, e);
        }

        private void frmDragObjects_MouseMove(object sender, MouseEventArgs e)
        {
            frmDragForm_MouseMove(sender, e);
        }

        #endregion FormDragwithMouse

        ////----------

        #region toolStripMenu

        private void StartupForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (PersianMessageBox.Show("آیا می‌خواهید خارج شوید؟   \n",
                                       "خروج",
                                       PersianMessageBox.Buttons.YesNo,
                                       PersianMessageBox.Icon.Question,
                                       PersianMessageBox.DefaultButton.Button2) == DialogResult.Yes)
            {
                try
                {
                    bgwCreatePPLib.CancelAsync();

                    System.Globalization.CultureInfo languageEn = new System.Globalization.CultureInfo("en-US");
                    InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(languageEn);
                }
                catch { }
            }
            else { e.Cancel = true; }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMinimized_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            try
            {
                Form _FormInfo = new FrmInfo();
                MPG_WinShutDowm.ShowShutForm ssf = new MPG_WinShutDowm.ShowShutForm(new FrmInfo());
                _FormInfo.Close();
            }
            catch { }
        }

        #endregion toolStripMenu

        ////----------

        #region toolStripLogo

        private void toolStripLogo_MouseHover(object sender, EventArgs e)
        {
            this.toolStripLogo.Cursor = System.Windows.Forms.Cursors.SizeAll;
        }

        #endregion toolStripLogo

        //----------
        #endregion Form Main

        #region Timer Status
        //----------

        private double iPersent = -1;
        private double iePersent = -1;

        private void StartTimerStatus()
        {
            try
            {
                ProgressBarStatusPersent.Value = 0;
                lblStatusPersentProcces.Text = "0 % Procces";
                lblStatusPersentTrueProcces.Text = "0 % True Procces";
                //
                dtStartProcess = DateTime.Now;
                tsEndToStart = dtStartProcess.Subtract(dtStartProcess);
                Invoke(new MethodInvoker(delegate () { lblTimerStatusLasted.Text = tsEndToStart.ToString(); }));

                timerCreatePPLib.Enabled = true;
            }
            catch { }
        }

        private void TimerStatusCounter()
        {
            try
            {
                tsEndToStart = DateTime.Now.Subtract(dtStartProcess);
                Invoke(new MethodInvoker(delegate () { lblTimerStatusLasted.Text = tsEndToStart.ToString(); }));
                //
                iPersent = ((SettingsMain.Default.CounterChf_int / SettingsMain.Default.SumChf_int) * 100);
                iPersent = (Math.Round(iPersent * (int)Math.Pow(10, SettingsMain.Default.xPow) / (int)Math.Pow(10, SettingsMain.Default.xPow)));
                Invoke(new MethodInvoker(delegate () { ProgressBarStatusPersent.Value = Convert.ToInt32(iPersent); }));
                Invoke(new MethodInvoker(delegate () { lblStatusPersentProcces.Text = iPersent + " % Procces"; }));
                //
                iePersent = (SettingsMain.Default.CounterChf_int - SettingsMain.Default.CounterErrorChf_int) / SettingsMain.Default.SumChf_int * 100;
                iePersent = (Math.Round(iePersent * (int)Math.Pow(10, SettingsMain.Default.xPow) / (int)Math.Pow(10, SettingsMain.Default.xPow)));
                Invoke(new MethodInvoker(delegate () { lblStatusPersentTrueProcces.Text = iePersent + " % True Procces"; }));
            }
            catch { }
        }

        private void timerCreatePPLib_Tick(object sender, EventArgs e)
        {
            try
            {
                trsTimer = new ThreadStart(TimerStatusCounter);
                trTimer = new Thread(trsTimer);
                trTimer.Priority = ThreadPriority.AboveNormal;
                //
                trTimer.Start();
            }
            catch { }
        }

        private void StopTimerStatus()
        {
            try
            {
                timerCreatePPLib.Enabled = false;
                //
                tsEndToStart = DateTime.Now.Subtract(dtStartProcess);
                Invoke(new MethodInvoker(delegate () { lblTimerStatusLasted.Text = tsEndToStart.ToString(); }));
                Invoke(new MethodInvoker(delegate () { lblStatusPersentProcces.Text = "100 % Procces"; }));
                //
                Invoke(new MethodInvoker(delegate () { ProgressBarStatusPersent.Value = 100; }));

                iePersent = (SettingsMain.Default.CounterChf_int - SettingsMain.Default.CounterErrorChf_int) / SettingsMain.Default.SumChf_int * 100;
                iePersent = (Math.Round(iePersent * (int)Math.Pow(10, SettingsMain.Default.xPow) / (int)Math.Pow(10, SettingsMain.Default.xPow)));
                Invoke(new MethodInvoker(delegate () { lblStatusPersentTrueProcces.Text = iePersent + " % True Procces"; }));
            }
            catch { }
        }

        //----------
        #endregion Timer Status

        #region Create New PPLibrary
        //----------

        private void bgwCreatePPLib_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            bool isErrorCNP = true;
            Invoke(new MethodInvoker(delegate () { isErrorCNP = isErrorCreateNewPPLibrary(); }));

            if (!isErrorCNP)
            {
                try
                {
                    if (PersianMessageBox.Show("آیا می‌خواهید « کلمات عبور »‌ جدیدی ثبت کنید؟   ",
                                               "ثبت کلمات عبور جدید",
                                               PersianMessageBox.Buttons.YesNo,
                                               PersianMessageBox.Icon.Question,
                                               PersianMessageBox.DefaultButton.Button1) == DialogResult.Yes)
                    {
                        int SelPAlgoritm = -1;
                        Invoke(new MethodInvoker(delegate () { SelPAlgoritm = cmbSelectedProtectedAlgoritm.SelectedIndex; }));

                        int tblLibPassCount = 0;

                        if (SelPAlgoritm == 0)
                        {
                            tblLibPassCount = cmbSelectedProtectedAlgoritm.Items.Count - 1;
                        }
                        else
                        {
                            tblLibPassCount = 1;
                        }

                        string ErrorGetSumChf_int = _PPLib.GetCreateSumChf_int(tblLibPassCount, Int32.Parse(numUDNumericsCount.Value.ToString()));

                        if (ErrorGetSumChf_int == "")
                        {
                            if (PersianMessageBox.Show("آیا می‌خواهید کلمات عبور جدید « تا " +
                                                       numUDNumericsCount.Value + " رقم » ثبت کنید؟   ",
                                                       "ثبت کلمات عبور",
                                                       PersianMessageBox.Buttons.YesNo,
                                                       PersianMessageBox.Icon.Question,
                                                       PersianMessageBox.DefaultButton.Button1) == DialogResult.Yes)
                            {
                                Invoke(new MethodInvoker(delegate () { tlpCreateLibPass.Enabled = false; }));

                                SettingsMain.Default.CEM_ErrorCreatePPLib = "";
                                SettingsMain.Default.CounterChf_int = 0;
                                SettingsMain.Default.CounterErrorChf_int = 0;

                                Invoke(new MethodInvoker(delegate () { StartTimerStatus(); }));

                                #region cmb Selected Protected Algoritm
                                //----------

                                if (SelPAlgoritm == 0)
                                {
                                    _PPLib.CreateMD5Library(Int32.Parse(numUDNumericsCount.Value.ToString()));
                                    _PPLib.CreateSHA1Library(Int32.Parse(numUDNumericsCount.Value.ToString()));
                                }

                                else if (SelPAlgoritm == 1)
                                {
                                    _PPLib.CreateMD5Library(Int32.Parse(numUDNumericsCount.Value.ToString()));
                                }

                                else if (SelPAlgoritm == 2)
                                {
                                    _PPLib.CreateSHA1Library(Int32.Parse(numUDNumericsCount.Value.ToString()));
                                }

                                //----------
                                #endregion cmb Selected Protected Algoritm

                                Invoke(new MethodInvoker(delegate () { StopTimerStatus(); }));

                                Invoke(new MethodInvoker(delegate () { tlpCreateLibPass.Enabled = true; }));

                                if (SettingsMain.Default.CEM_ErrorCreatePPLib == "")
                                {
                                    PersianMessageBox.Show("ثبت « کلمات عبور »‌ جدید با موفقیت انجام شد. ",
                                                           "ثبت کلمات عبور",
                                                           PersianMessageBox.Buttons.OK,
                                                           PersianMessageBox.Icon.Information,
                                                           PersianMessageBox.DefaultButton.Button1);
                                }
                                else
                                {
                                    if (PersianMessageBox.Show("ثبت « کلمات عبور »‌ جدید با  خطا همراه بود.      \n" +
                                                              "\n مایل به مشاهده جزئیات خطا هستید؟ ",
                                                              "خطا",
                                                              PersianMessageBox.Buttons.YesNo,
                                                              PersianMessageBox.Icon.Error,
                                                              PersianMessageBox.DefaultButton.Button1) == DialogResult.Yes)
                                    {
                                        try
                                        {
                                            SettingsMain.Default.CEM_MesErrorsForm_NotFriendly = SettingsMain.Default.CEM_ErrorCreatePPLib_NotFriendly;
                                            SettingsMain.Default.CEM_MesErrorsForm = SettingsMain.Default.CEM_ErrorCreatePPLib;

                                            FrmErrors _FormErrors = new FrmErrors();
                                            _FormErrors.ShowDialog();
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                        else
                        {
                            PersianMessageBox.Show(ErrorGetSumChf_int,
                                                   "خطا",
                                                   PersianMessageBox.Buttons.OK,
                                                   PersianMessageBox.Icon.Error,
                                                   PersianMessageBox.DefaultButton.Button1);
                        }
                    }
                }
                catch { }
            }
            else
            {
                PersianMessageBox.Show(SettingsMain.Default.MessageCreatePPLibError,
                                       "خطا",
                                       PersianMessageBox.Buttons.OK,
                                       PersianMessageBox.Icon.Error,
                                       PersianMessageBox.DefaultButton.Button1);
            }
        }

        private bool isErrorCreateNewPPLibrary()
        {
            bool isError = false;
            SettingsMain.Default.MessageCreatePPLibError = "قسمت‌های زیر را تکمیل کنید:      \n";

            try
            {
                if (cmbSelectedProtectedAlgoritm.SelectedItem == null)
                {
                    SettingsMain.Default.MessageCreatePPLibError += "\n الگوریتم رمزنگاری را انتخاب کنید. ";
                    isError = true;
                }
            }
            catch { isError = true; }

            return isError;
        }

        private void btnCreatePPLib_Click(object sender, EventArgs e)
        {
            try
            {
                trsCreatePPLib = new ThreadStart(bgwCreatePPLib.RunWorkerAsync);
                trCreatePPLib = new Thread(trsCreatePPLib);
                trCreatePPLib.Priority = ThreadPriority.Normal;
                //
                trCreatePPLib.Start();
            }
            catch { }
        }

        //----------
        #endregion Create New PPLibrary

        #region Form Search Library
        //----------

        private void ShowSearchLibrary()
        {
            try
            {
                FrmSearch _FormSearch = new FrmSearch();

                Invoke(new MethodInvoker(delegate () { this.Hide(); }));
                _FormSearch.ShowDialog();
                Invoke(new MethodInvoker(delegate () { this.Show(); }));
            }
            catch { }
        }

        private void btnSearchLibrary_Click(object sender, EventArgs e)
        {
            try
            {
                trsSearchLibrary = new ThreadStart(ShowSearchLibrary);
                trSearchLibrary = new Thread(trsSearchLibrary);
                trSearchLibrary.Priority = ThreadPriority.Normal;
                trSearchLibrary.SetApartmentState(ApartmentState.STA);
                //
                trSearchLibrary.Start();
            }
            catch { }
        }

        //----------
        #endregion Form Search Library

        //------------
        ////----------////---------------------------------------------------------------------// Begin FormStartup_Load
        //------------

        private void StartupForm_Load(object sender, EventArgs e)
        {
            string MessageError = string.Empty;

            try
            {
                this.TopMost = true;
                this.TopMost = false;
            }
            catch
            {
                MessageError = "\nفرآیند بارگیری برنامه با خطا همراه بود.   \n" +
                               "\nمتاسفیم.   \n";

                PersianMessageBox.Show(MessageError,
                                   "خطا",
                                   PersianMessageBox.Buttons.OK,
                                   PersianMessageBox.Icon.Error,
                                   PersianMessageBox.DefaultButton.Button1);
            }
        }

        //------------
        ////----------////---------------------------------------------------------------------// End FormStartup_Load
        //------------
    }
}