﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Protected Passwords Library - hIVE")]
[assembly: AssemblyDescription("کتابخانه کلمات عبور حمایت‌شده - کندو")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("hIVE Productions")]
[assembly: AssemblyProduct("PPLib-hIVE")]
[assembly: AssemblyCopyright("Copyright ©  2015 hIVE Productions Inc. All Rights Reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("b4b45f35-443a-4795-8c56-30f0e2b1d056")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.00.51221")]
[assembly: AssemblyFileVersion("1.00.51221")]
[assembly: NeutralResourcesLanguageAttribute("en")]
